from typing import Callable

from sqlalchemy.ext.asyncio import AsyncSession, AsyncEngine
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import sessionmaker


def get_async_engine(async_db_url: str) -> AsyncEngine:
    """
    returns SQLAlchemy AsyncEngine

    Args:
        async_db_url (str): url to db. Must contain async driver!

    Returns:
        AsyncEngine: SQLAlchemy AsyncEngine
    """

    return create_async_engine(async_db_url)


def get_async_session_pool(engine: AsyncEngine) -> Callable[[], AsyncSession]:
    return sessionmaker(
        engine, class_=AsyncSession, expire_on_commit=True
    )

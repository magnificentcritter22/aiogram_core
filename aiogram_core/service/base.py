from typing import Any, Type, TypeVar

from httpx import AsyncClient, Client
from sqlalchemy.ext.asyncio import AsyncSession

from ..defaults.containers import BaseContainer


T = TypeVar('T')


class BaseService:
    """

    Basic class with gateway factory.

    """

    def __init__(
            self,
            container: Any | None = None
    ) -> None:
        if container:
            self._container = container
        else:
            self._container = BaseContainer()

    def gateway_factory(self, cls: Type[T], client: AsyncClient | Client) -> T:

        placeholder = f'_{cls.__name__}'
        if not hasattr(self._container, placeholder):
            setattr(self._container, placeholder, cls(client))

        gateway: cls = getattr(self._container, placeholder)
        return gateway


class BaseServiceWithSession(BaseService):
    def __init__(
            self,
            session: AsyncSession,
            container: Any | None = None,
    ):
        """
        Extension of BaseService with AsyncSession repository

        Args:
            session (AsyncSession): async database sesion
        """
        
        self.session = session
        super().__init__(container)

    def repo_factory(self, cls: Type[T]) -> T:
        placeholder = f"_{cls.__name__}"
        if not hasattr(self._container, placeholder):
            setattr(self._container, placeholder, cls(self.session))

        repo: cls = getattr(self._container, placeholder)
        return repo

    def db_service_factory(self, cls: Type[T]) -> T:
        """
        DB service factory. Used if no gateway is used and just propagates
        the session object further to db service...
        
        NOTE:
            Just DB session propagates to a service class. Container doesn't propagate

        Args:
            cls (Type[T]): db service class

        Returns:
            T: constructed class cls(Type[T])
        """

        placeholder = f"_{cls.__name__}"
        if not hasattr(self._container, placeholder):
            # NOTE: is there a point to use container ?
            setattr(self._container, placeholder, cls(self.session))

        repo: cls = getattr(self._container, placeholder)
        return repo

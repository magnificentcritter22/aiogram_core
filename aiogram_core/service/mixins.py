from aiogram.types import (
    Message,
    ExternalReplyInfo,
)

from aiogram_core.exceptions import (
    NoReplyException,
    NoCommandArgsException,
)


class GetMessageArgsMixin:

    @staticmethod
    def get_arg(message: Message) -> str:  # TODO: move to BaseService
        message_splitted = message.text.split(' ', maxsplit=1)
        if len(message_splitted) == 1:
            raise NoCommandArgsException('No argument for this command was provided')
        return message_splitted[1]
    
    @staticmethod
    def get_args(message: Message) -> list[str]:
        message_splitted = message.text.split(' ', maxsplit=1)
        if len(message_splitted) == 1:
            raise NoCommandArgsException('No argument(-s) for this command were provided')
        return message_splitted[1:]


class GetMessageReplyMixin:

    @staticmethod
    def get_replied_message(
        message: Message,
        raise_exception: bool = True
    ) -> Message | ExternalReplyInfo | None:

        """
        gets message for which argument message is reply to

        NOTE:
            reply_to_message and external reply do not contain further <replly to> object, so
            calling this method twice wont return anything

        Args:
            message (Message): aiogram Message object.

        Returns:
            aiogram Message object | ExternalReplyInfo object or NoneType object: replied message
        """

        try:
            reply = message.reply_to_message or message.external_reply
        except AttributeError:
            reply = None

        if reply or not raise_exception:
            return reply
        
        raise NoReplyException('No reply for this message found...')

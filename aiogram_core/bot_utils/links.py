from aiogram.types import Message, ExternalReplyInfo
from aiogram_core.constants import ChatTypeEnum


class LinkBuilder:

    @staticmethod
    def message_link(
        message: Message | ExternalReplyInfo,
        link_name: str | None = None,
        N: int = 10
    ):
        """
        Builds hyperlink <a href="link"> link_name </a> for message link
        
        link template is "https://t.me/c/{channel_id}/{id}" for channels and (super-)groups
        
        Docs: https://core.telegram.org/api/links#message-links

        If link_name is None, takes N first characters from message text (if they are present) 
        and builds link_name='<text>...'

        Args:
            message (Message | ExternalReplyInfo): aiogram Message object
            link_name (str | None, optional): link_name for hyperlink. Defaults to None.

        Returns:
            _type_: _description_
        """
        if link_name is None:
            link_name = message.text[:10] if message.text else 'message'
        
        if message.chat.type in [ChatTypeEnum.GROUP, ChatTypeEnum.SUPERGROUP, ChatTypeEnum.CHANNEL]:
            link = LinkBuilder._supergroup_channel_message_link(message)
        
        if link:
            return LinkBuilder._html_link_builder(
                link=link,
                link_name=link_name,
            )

    @staticmethod
    def _supergroup_channel_message_link(message: Message):
        return 'https://t.me/c/{}/{}'.format(
            message.chat.shifted_id,
            message.message_id,
        )

    @staticmethod
    def _html_link_builder(link: str, link_name: str):
        return "<a href='{}'>{}</a>".format(
            link,
            link_name,
        )

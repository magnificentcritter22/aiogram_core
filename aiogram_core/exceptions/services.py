

class NoReplyException(Exception):

    def __init__(self, message: str = 'No reply provided') -> None:
        self.message = message
        super().__init__(message)

    def __str__(self) -> str:
        return self.message


class NoCommandArgsException(Exception):
    def __init__(self, message: str = 'No argument(-s) provided') -> None:
        self.message = message
        super().__init__(message)

    def __str__(self) -> str:
        return self.message

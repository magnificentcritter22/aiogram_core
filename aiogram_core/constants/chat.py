from enum import Enum


class ChatTypeEnum(str, Enum):
    PRIVATE: str = 'private'
    GROUP: str = 'group'
    SUPERGROUP: str = 'supergroup'
    CHANNEL: str = 'channel'
